const int lm35 = 0;
float temperatura = 0;
int adc = 0;
const int Buzzer = 12;

void setup(){
  analogReference(INTERNAL);  
  pinMode(Buzzer, OUTPUT);
}

void loop(){
  adc = analogRead(lm35); 
  temperatura = adc * 0.1075268817;
  if (temperatura > 25)
    digitalWrite(Buzzer, HIGH);
  else
    digitalWrite(Buzzer, LOW);
}
