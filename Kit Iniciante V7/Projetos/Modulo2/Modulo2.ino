const int ledRed = 13;
const int ledYellow = 12;
const int ledGreen = 11;
const int BotaoRed = 2;
const int BotaoYellow = 3;
const int BotaoGreen = 4;
int EstadoBtRed = 0;
int EstadoBtYellow = 0;
int EstadoBtGreen = 0;

void setup(){
  pinMode(ledRed, OUTPUT);
  pinMode(ledYellow, OUTPUT);
  pinMode(ledGreen, OUTPUT);
  pinMode(BotaoRed, INPUT);
  pinMode(BotaoYellow, INPUT);
  pinMode(BotaoGreen, INPUT);
} 

void loop(){
  EstadoBtRed = digitalRead(BotaoRed);
  EstadoBtYellow = digitalRead(BotaoYellow); 
  EstadoBtGreen = digitalRead(BotaoGreen);
  
  if(EstadoBtRed == HIGH)
    digitalWrite(ledRed, HIGH);
  else
    digitalWrite(ledRed, LOW);
  
  if(EstadoBtYellow == HIGH)
    digitalWrite(ledYellow, HIGH);
  else
    digitalWrite(ledYellow, LOW);
    
  if(EstadoBtGreen == HIGH)
    digitalWrite(ledGreen, HIGH);
  else
    digitalWrite(ledGreen, LOW);
}
