const int Potenciometro = A0;
const int led = 13;
int ValorPot = 0;

void setup(){
  pinMode(led, OUTPUT); 
}

void loop(){
  ValorPot = analogRead(Potenciometro);
  digitalWrite(led, HIGH);
  delay(ValorPot);
  digitalWrite(led, LOW);
  delay(ValorPot);
}
