#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
const int lm35 = 0;
int adcLido = 0;
float fahrenheit = 0;
float celsius = 0;

void setup(){
  Serial.begin(9600);
  analogReference(INTERNAL);
  lcd.begin(16,2);
}

void loop(){
  adcLido = analogRead(lm35);
  celsius = adcLido * 0.1075268817204301;
  fahrenheit = (celsius * 1.8) + 32;
  lcd.setCursor(0,0);
  lcd.print("Celsius: ");
  lcd.print(celsius);
  lcd.setCursor(0,1);
  lcd.print("Fahrenheit: ");
  lcd.print(fahrenheit);
  delay(1000);
}
