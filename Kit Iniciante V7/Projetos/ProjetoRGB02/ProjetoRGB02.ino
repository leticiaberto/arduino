const int R = 9;
const int G = 10;
const int B = 11;
int ValRed = 255;
int ValGreen = 0;
int ValBlue = 0;

void setup(){
  pinMode(R, OUTPUT);
  pinMode(G, OUTPUT);
  pinMode(B, OUTPUT);
  analogWrite(R, ValRed);
  analogWrite(G, ValGreen);
  analogWrite(B, ValBlue);
}

void loop(){
  for(ValGreen = 0; ValGreen < 255; ValGreen = ValGreen + 5){
    analogWrite(G, ValGreen);
    delay(50);
  }
  
  for(ValRed = 255; ValRed > 0; ValRed = ValRed - 5){
    analogWrite(R, ValRed);
    delay(50);
  }
  
  for(ValBlue = 0; ValBlue < 255; ValBlue = ValBlue - 5){
    analogWrite(B, ValBlue);
    delay(50);
  }
  
  for(ValGreen = 255; ValGreen > 0; ValGreen = ValGreen - 5){
    analogWrite(G, ValGreen);
    delay(50);
  }
  
  for(ValRed = 0; ValRed < 255; ValRed = ValRed + 5){
    analogWrite(R, ValRed);
    delay(50);
  }
  
  for(ValBlue = 255; ValBlue > 0; ValBlue = ValBlue - 5){
    analogWrite(B, ValBlue);
    delay(50);
  }
}
