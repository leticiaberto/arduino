int LM35 = 0;
int temp = 0;
float celsius = 0;
float fahrenheit = 0;

void setup(){
  Serial.begin(9600);
  analogReference(INTERNAL);
}

void loop(){
  temp = analogRead(LM35);
  celsius = temp * 0.1075268817204301;
  fahrenheit = (celsius * 1.8) + 32;
  
  Serial.print("Temperatura em Fahrenheit: ");
  Serial.println(celsius);
  delay(1000);
}
