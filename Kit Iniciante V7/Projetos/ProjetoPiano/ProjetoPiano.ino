const int ledRed = 13;
const int ledYellow = 12;
const int ledGreen = 11;
const int BotaoRed = 2;
const int BotaoYellow = 3;
const int BotaoGreen = 4;
const int Buzzer = 10;
int EstadoBotaoRed = 0;
int EstadoBotaoYellow = 0;
int EstadoBotaoGreen = 0;
int Tom = 0;

void setup(){
  pinMode(ledRed, OUTPUT);
  pinMode(ledYellow, OUTPUT);
  pinMode(ledGreen, OUTPUT);
  pinMode(BotaoRed, INPUT);
  pinMode(BotaoYellow, INPUT);
  pinMode(BotaoGreen, INPUT);
  pinMode(Buzzer, OUTPUT);
}

void loop(){
  EstadoBotaoRed = digitalRead(BotaoRed);
  EstadoBotaoYellow = digitalRead(BotaoYellow);
  EstadoBotaoGreen = digitalRead(BotaoGreen);
  
  if(EstadoBotaoRed && !EstadoBotaoYellow && !EstadoBotaoGreen){
    Tom = 100;
   digitalWrite(ledRed, HIGH); 
  }
  if(!EstadoBotaoRed && EstadoBotaoYellow && !EstadoBotaoGreen){
    Tom = 200;
    digitalWrite(ledYellow, HIGH);
  }
  if(!EstadoBotaoRed && !EstadoBotaoYellow && EstadoBotaoGreen){
    Tom = 500;
    digitalWrite(ledGreen, HIGH);
  }
  
  if(Tom > 0){
    digitalWrite(Buzzer, HIGH);
    delayMicroseconds(TOM);
    digitalWrite(Buzzer, LOW);
    delayMicroseconds(TOM);
    Tom = 0; 
    digitalWite(ledRed, LOW);
    digitalWrite(ledYellow, LOW);
    digitalWrite(ledGreen, LOW);
  }
}
