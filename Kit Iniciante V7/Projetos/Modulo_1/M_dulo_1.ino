const int pinLed = 13;
const int Botao = 2;
int EstadoBotao = 0;

void setup(){
  pinMode(pinLed, OUTPUT);
  pinMode(Botao, INPUT);
}

void loop(){
  EstadoBotao = digitalRead(Botao);
  
  if(EstadoBotao == HIGH)
    digitalWrite(pinLed, HIGH);
  else
    digitalWrite(pinLed, LOW);
} 
