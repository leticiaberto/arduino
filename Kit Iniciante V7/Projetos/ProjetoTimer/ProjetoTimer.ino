int segundo = 0;
int minuto = 0;
const int led = 13;
const int Buzzer = 5;
const int Bt1 = 2;
const int Bt2 = 3;
const int Bt3 = 4;
int EstadoBt1 = 0;
int EstadoBt2 = 0;
int EstadoBt3 = 0;

void setup(){
  pinMode(Buzzer, OUTPUT);
  pinMode(led, OUTPUT);
  pinMode(Bt1, INPUT);
  pinMode(Bt2, INPUT);
  pinMode(Bt3, INPUT);
  Serial.begin(9600);
  Serial.println("Selecione o tempo do timer...");
  Serial.println("Minutos: 0");
  Serial.println("Segundos: 0");
}

void loop(){
  EstadoBt1 = digitalRead(Bt1);
  if(EstadoBt1 == HIGH){
    delay(250);
    if(EstadoBt1 == HIGH){
      segundo++;
      if(segundo >= 60)
        segundo = 0;
      Serial.print("Segundos: ");
      Serial.println(segundo);
    }
  }
  
  EstadoBt2 = digitalRead(Bt2);
  if(EstadoBt2 == HIGH){
    delay(250);
    if(EstadoBt2 == HIGH){
      minuto++;
      if(minuto >= 60)
        minuto = 0;
      Serial.print("Minutos: ");
      Serial.println(minuto);
    }
  }
  
  EstadoBt3 = digitalRead(Bt3);
  if(EstadoBt3 == HIGH){
    if((minuto == 0) && (segundo == 0)){
      //não faz nada
    }
    else{
      Serial.println("START");
      delay(1000);
      
      if(segundo == 0){
        minuto--;
        segundo = 59;
      }
      
      for(int y = 0; y < segundo; y--){
        if(minuto < 10){
          Serial.print("0");
          Serial.print(minuto);
          Serial.print(":");
        }
        else{
          Serial.print(minuto);
          Serial.print(":");
        }
        
        if(segundo < 10){
          Serial.print("0");
          Serial.println(segundo);
        }
        else
          Serial.println(segundo);
        
        segundo--;
        
        if(segundo < 0){
          minuto--;
          segundo = 59;
        }
        
        delay(1000);
        
        if((segundo <= 0) && (minuto <= 0)){
          Serial.println("ACABOU O TEMPO!");
          
          digitalWrite(Buzzer, HIGH);
          delay(250);
          digitalWrite(Buzzer, LOW);
          delay(100);
          digitalWrite(Buzzer, HIGH);
          delay(250);
          digitalWrite(Buzzer, LOW);
          delay(100);
          digitalWrite(Buzzer, HIGH);
          delay(250);
          digitalWrite(Buzzer, LOW);
          delay(100);
          digitalWrite(Buzzer, HIGH);
          delay(1000);
          digitalWrite(Buzzer, LOW);
          break;
        }
      }
      Serial.println("Selecione o tempo do timer...");
    }
  }
}
