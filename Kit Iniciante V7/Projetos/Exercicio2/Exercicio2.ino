int LED[] = {13, 12, 11, 10, 9, 8, 7, 6, 5, 4};

void setup(){
  for(int x = 0; x < 10; x++)
    pinMode(LED[x], OUTPUT);
}
void loop(){
  for(int x = 0; x < 10; x++){
    digitalWrite(LED[x], HIGH);
    delay(1000);
  }
  for(int x = 9; x >= 0; x--){
    digitalWrite(LED[x], LOW);
    delay(1000);
  }
}
