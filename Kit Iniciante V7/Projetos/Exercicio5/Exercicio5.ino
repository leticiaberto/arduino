const int A = 12;
const int B = 11;
const int C = 10;
const int D = 9;
const int E = 8;
const int F = 6;
const int G = 7;

const int Bt = 4;
int EstadoBt = 0;
int num = 0;

void setup(){
  pinMode(A, OUTPUT);
  pinMode(B, OUTPUT);
  pinMode(C, OUTPUT);
  pinMode(D, OUTPUT);
  pinMode(E, OUTPUT);
  pinMode(F, OUTPUT);
  pinMode(G, OUTPUT);
  pinMode(Bt, INPUT);
}

void loop(){
  EstadoBt = digitalRead(Bt);
  if(EstadoBt == HIGH){
    delay(100);
    //para confirmar que apertou o botao
    if(EstadoBt == HIGH){
      num = random(1, 7);
      switch(num){
        case 1:
         digitalWrite(A, LOW);     
         digitalWrite(B, HIGH);
         digitalWrite(C, HIGH);
         digitalWrite(D, LOW);
         digitalWrite(E, LOW);
         digitalWrite(F, LOW);
         digitalWrite(G, LOW);
         break;
        case 2:
         digitalWrite(A, HIGH);     
         digitalWrite(B, HIGH);
         digitalWrite(C, LOW);
         digitalWrite(D, HIGH);
         digitalWrite(E, HIGH);
         digitalWrite(F, LOW);
         digitalWrite(G, HIGH);
         break;
       case 3:
         digitalWrite(A, HIGH);     
         digitalWrite(B, HIGH);
         digitalWrite(C, HIGH);
         digitalWrite(D, HIGH);
         digitalWrite(E, LOW);
         digitalWrite(F, LOW);
         digitalWrite(G, HIGH);
         break;
       case 4:
         digitalWrite(A, LOW);     
         digitalWrite(B, HIGH);
         digitalWrite(C, HIGH);
         digitalWrite(D, LOW);
         digitalWrite(E, LOW);
         digitalWrite(F, HIGH);
         digitalWrite(G, HIGH);
         break;
       case 5:
         digitalWrite(A, HIGH);     
         digitalWrite(B, LOW);
         digitalWrite(C, HIGH);
         digitalWrite(D, HIGH);
         digitalWrite(E, LOW);
         digitalWrite(F, HIGH);
         digitalWrite(G, HIGH);
         break;
       case 6:
         digitalWrite(A, LOW);     
         digitalWrite(B, LOW);
         digitalWrite(C, HIGH);
         digitalWrite(D, HIGH);
         digitalWrite(E, HIGH);
         digitalWrite(F, HIGH);
         digitalWrite(G, HIGH);
         break;
      }
    }
  }
  //se nao apertar o botao nao faz nada
  else{
  }
}
