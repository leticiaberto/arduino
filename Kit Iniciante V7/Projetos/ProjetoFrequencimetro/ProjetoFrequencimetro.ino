#include <LiquidCrystal.h>

int freq = 0;
int Bt1 = 8;
int Bt2 = 9;
int EstadoBt1 = 0;
int EstadoBt2 = 0;
int Buzzer = 6;
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup(){
  lcd.begin(16, 2);
  lcd.setCursor(0,0);
  lcd.print("Frequencia = ");
  lcd.setCursor(6, 1);
  lcd.print("Hz");
  pinMode(Bt1, INPUT);
  pinMode(Bt2, INPUT);
  pinMode(Buzzer, OUTPUT);
}

void loop(){
  EstadoBt1 = digitalRead(Bt1);
  EstadoBt2 = digitalRead(Bt2);
  
  if(EstadoBt1 == HIGH)
    freq = freq + 100;
    
  if(EstadoBt2 == HIGH)
    freq = freq - 100;
    
  if(freq <= 0)
    freq = 0;
    
  if(freq >=20000)
    freq = 20000;

  if(freq <= 99){
    lcd.setCursor(0,1);
    lcd.print(" ");
    lcd.setCursor(1,1);
    lcd.print(" ");
    lcd.setCursor(2,1);
    lcd.print(" ");
    lcd.setCursor(3,1);
  }    
  
  if(freq >= 100){
    lcd.setCursor(0,1);
    lcd.print(" ");
    lcd.setCursor(1,1);
  }
  
  if(freq >= 10000)
    lcd.setCursor(0,1);
    
  lcd.print(freq);
  tone(Buzzer, freq);
  
  delay(100);
}
