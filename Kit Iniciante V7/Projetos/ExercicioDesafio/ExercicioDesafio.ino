#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 7, 6, 5, 4);

const int Bt1 = 2;
const int Bt2 = 3;
int EstadoBt1 = 0;
int EstadoBt2 = 0;
int velocidade = 1000;
int sentido = 0; // Zero a bola vai, 1 a bola vem
int i, x, k = 0;

void setup(){
  Serial.begin(9600);
  
  pinMode(Bt1, INPUT);
  pinMode(Bt2, INPUT);
  attachInterrupt(0, rebateuDireita, RISING); //interrupção no pino 2
  attachInterrupt(1, rebateuEsquerda, RISING); //interrupção no pino 3
  lcd.begin(16,2);
  lcd.print("Ping Pong");
  delay(2000);
  lcd.clear();
}
  
void loop(){
  jogo();
}

void jogo(){
  while(1){
    //da direita pra esquerda
    if(sentido == 0){
      for(i = 15; i>= 0; i--){
        switch(k){
          case 0:
            lcd.setCursor(i, 0);
            lcd.print("o");
            delay(velocidade);
            lcd.clear();
            k = 1;
            break;
            
         case 1:
           lcd.setCursor(i,1);
           lcd.print("o");
           delay(velocidade);
           lcd.clear();
           k = 0;
           break;
        }
      }
    }
    
    //da esquerda pra direita
    if(sentido == 1){
      for(i = 0; i < 16; i++){
        switch(k){
          case 0:
            lcd.setCursor(i, 0);
            lcd.print("o");
            delay(velocidade);
            lcd.clear();
            k = 1;
            break;
          
          case 1:
            lcd.setCursor(i,1);
            lcd.print("o");
            delay(velocidade);
            lcd.clear();
            k = 0;
            break;
        }
      }   
    }
  }
}

void rebateuEsquerda(){
  //bola no canto esquerdo
  if(i == 0){
    sentido = 1;
    velocidade = velocidade - 50;//aumenta a velocidade
    if(velocidade <= 50)
      velocidade = 50;//velocidade minima
  }
  else
    fimdeJogo(0);//mostra que o ESQUERDA perdeu
}

void rebateuDireita(){
  //bola no canto direito
  if(i == 15){
    sentido = 0;
    velocidade = velocidade - 50;
    if (velocidade <= 50)
      velocidade = 50;
  }
  else
    fimdeJogo(1);//mostra que a DIREITA perdeu;
}

int fimdeJogo(int x){
  if(x == 0){
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Esquerda perdeu");
    lcd.setCursor(0,1);
    lcd.print("Resete o Arduino");
    while(1);
  }
  
  if(x == 1){
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Direita perdeu");
    lcd.setCursor(0,1);
    lcd.print("Resete o Arduino");
    while(1);
  }
}
