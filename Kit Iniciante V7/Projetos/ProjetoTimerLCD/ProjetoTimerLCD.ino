#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

int segundo = 0;
int minuto = 0;
const int led = 13;
const int Buzzer = A2;
const int Bt1 = A5;
const int Bt2 = A4;
const int Bt3 = A3;
int EstadoBt1 = 0;
int EstadoBt2 = 0;
int EstadoBt3 = 0;

void setup(){
  pinMode(Bt1, INPUT);
  pinMode(Bt2, INPUT);
  pinMode(Bt3, INPUT);
  pinMode(led, OUTPUT);
  pinMode(Buzzer, OUTPUT);
  lcd.begin(16, 2);
  inicializacao();
}

void loop(){
  EstadoBt1 = digitalRead(Bt1);
  if(EstadoBt1 == HIGH){
    delay(150);
    segundo++;
    if(segundo >= 60){
      segundo = 0;
      lcd.setCursor(11,0);
      lcd.print(" ");
    }
    
    lcd.setCursor(0,0);
    lcd.print("Segundos: ");
    lcd.print(segundo);
  }
  
  EstadoBt2 = digitalRead(Bt2);
  if(EstadoBt2 == HIGH){
    delay(150);
    if(EstadoBt2 == HIGH){
      minuto++;
      if(minuto >= 60){
        minuto = 0;
        lcd.setCursor(10,1);
        lcd.print(" ");
      }
      lcd.setCursor(0,1);
      lcd.print("Minutos: ");
      lcd.print(minuto);
    }
  }
  
  EstadoBt3 = digitalRead(Bt3);
  if(EstadoBt3 == HIGH){
    if((minuto == 0) && (segundo == 0)){
      //nao faz nada
    }
    else{
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("START");
      delay(1000);
      
      if(segundo == 0){
        minuto--;
        segundo = 59;
      }
      
      for(int y = 0; y < segundo; y--){
        if(minuto < 10){
          lcd.setCursor(5,1);
          lcd.print("0");
          lcd.print(minuto);
          lcd.print(":");
        }
        else{
          lcd.setCursor(5,1);
          lcd.print(minuto);
          lcd.print(":");
        }
        
        if(segundo < 10){
          lcd.print("0");
          lcd.print(segundo);
        }
        else
          lcd.print(segundo);
          
        segundo--;
        
        if(segundo < 0){
          minuto--;
          segundo = 59;
        }
        
        delay(1000);
        
        if((segundo <= 0) && (minuto <= 0)){
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("ACABOU O TEMPO!");
          
          digitalWrite(Buzzer, HIGH);
          delay(250);
          digitalWrite(Buzzer, LOW);
          delay(100);
          digitalWrite(Buzzer, HIGH);
          delay(250);
          digitalWrite(Buzzer, LOW);
          delay(100);
          digitalWrite(Buzzer, HIGH);
          delay(250);
          digitalWrite(Buzzer, LOW);
          delay(100);
          digitalWrite(Buzzer, HIGH);
          delay(1000);
          digitalWrite(Buzzer, LOW);
          break;
        }
      }
      inicializacao();
    }
  }
}

void inicializacao(){
  lcd.begin(16,2);
  lcd.print("Selecione o ");
  lcd.setCursor(0,1);
  lcd.print("tempo do timer..");
  delay(1500);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Segundos: ");
  lcd.print(segundo);
  lcd.setCursor(0,1);
  lcd.print("Minutos: ");
  lcd.print(minuto);
}
