const int LED[] = {12, 11, 10, 9, 8, 7, 6, 5, 4, 3};
const int Brilho = 2;
const int Potenciometro = 0;
int ValorPot = 0;
int pwm = 0;

void setup(){
  for(int i = 0; i < 10; i++){
    pinMode(LED[i], OUTPUT);
  }
  pinMode(Brilho, OUTPUT);
}

void loop(){
  ValorPot = analogRead(Potenciometro);
  pwm = map(ValorPot, 0, 1023, 0, 255);
  analogWrite(Brilho, pwm);
  
  if(pwm > 5)
    digitalWrite(LED[0], HIGH);
  else
    digitalWrite(LED[0], LOW);
    
  if(pwm > 50)
    digitalWrite(LED[1], HIGH);
  else
    digitalWrite(LED[1], LOW);
    
  if(pwm > 75)
    digitalWrite(LED[2], HIGH);
  else
    digitalWrite(LED[2], LOW);
    
  if(pwm > 120)
    digitalWrite(LED[3], HIGH);
  else
    digitalWrite(LED[3], LOW);
    
  if(pwm > 200)
    digitalWrite(LED[4], HIGH);
  else
    digitalWrite(LED[4], LOW);
    
  if(pwm > 300)
    digitalWrite(LED[5], HIGH);
  else
    digitalWrite(LED[5], LOW);
    
  if(pwm > 400)
    digitalWrite(LED[6], HIGH);
  else
    digitalWrite(LED[6], LOW);
    
  if(pwm > 470)
    digitalWrite(LED[7], HIGH);
  else
    digitalWrite(LED[7], LOW);
    
  if(pwm > 530)
    digitalWrite(LED[8], HIGH);
  else
    digitalWrite(LED[8], LOW);
    
  if(pwm > 670)
    digitalWrite(LED[9], HIGH);
  else
    digitalWrite(LED[9], LOW);
}
