const int led = 6;
int valor = 0;
const int LDR = 0;
int pwm = 0;
void setup(){
  pinMode(led, OUTPUT);
}

void loop(){
  valor = analogRead(LDR);
  
  if (valor < 500){
    analogWrite(led, pwm);
    pwm++;
    delay(100);
  }
  else{
    digitalWrite(led, LOW);
    pwm = 0;
  }
  
  if (pwm > 255)
    pwm = 255;
}
