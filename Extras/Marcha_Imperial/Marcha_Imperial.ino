int led = 13;
int Buzzer = 9;

//frequencies for the tones we're going to use
#define c 261
#define d 294
#define e 329
#define f 349
#define g 391
#define gS 415
#define a 440
#define aS 455
#define b 466
#define cH 523
#define cSH 554
#define dH 587
#define dSH 622
#define eH 659
#define fH 698
#define fSH 740
#define gH 784
#define gSH 830
#define aH 880

void setup(){
  pinMode(led, OUTPUT);
  pinMode(Buzzer, OUTPUT);
}

void loop(){
  march();
}

void beep(unsigned char Buzzer, int frequencyInHertz, long timeInMilliseconds){
  digitalWrite(led, HIGH);
  
  int x;
  long delayAmount = (long)(1000000/frequencyInHertz);
  long loopTime = (long)((timeInMilliseconds*1000)/(delayAmount*2));
  
  for (x = 0; x < loopTime; x++){
    digitalWrite(Buzzer, HIGH);
    delayMicroseconds(delayAmount);
    digitalWrite(Buzzer, LOW);
    delayMicroseconds(delayAmount); 
  }
  digitalWrite(led, LOW);
  delay(20);
}

void march(){
    beep(Buzzer, a, 500);
    beep(Buzzer, a, 500);
    beep(Buzzer, f, 350);
    beep(Buzzer, cH, 150);
 
    beep(Buzzer, a, 500);
    beep(Buzzer, f, 350);
    beep(Buzzer, cH, 150);
    beep(Buzzer, a, 1000);
    //first bit
 
    beep(Buzzer, eH, 500);
    beep(Buzzer, eH, 500);
    beep(Buzzer, eH, 500);
    beep(Buzzer, fH, 350);
    beep(Buzzer, cH, 150);
 
    beep(Buzzer, gS, 500);
    beep(Buzzer, f, 350);
    beep(Buzzer, cH, 150);
    beep(Buzzer, a, 1000);
    //second bit...
 
    beep(Buzzer, aH, 500);
    beep(Buzzer, a, 350);
    beep(Buzzer, a, 150);
    beep(Buzzer, aH, 500);
    beep(Buzzer, gSH, 250);
    beep(Buzzer, gH, 250);
 
    beep(Buzzer, fSH, 125);
    beep(Buzzer, fH, 125);
    beep(Buzzer, fSH, 250);
    delay(250);
    beep(Buzzer, aS, 250);
    beep(Buzzer, dSH, 500);
    beep(Buzzer, dH, 250);
    beep(Buzzer, cSH, 250);
    //start of the interesting bit
 
    beep(Buzzer, cH, 125);
    beep(Buzzer, b, 125);
    beep(Buzzer, cH, 250);
    delay(250);
    beep(Buzzer, f, 125);
    beep(Buzzer, gS, 500);
    beep(Buzzer, f, 375);
    beep(Buzzer, a, 125);
 
    beep(Buzzer, cH, 500);
    beep(Buzzer, a, 375);
    beep(Buzzer, cH, 125);
    beep(Buzzer, eH, 1000);
    //more interesting stuff (this doesn't quite get it right somehow)
 
    beep(Buzzer, aH, 500);
    beep(Buzzer, a, 350);
    beep(Buzzer, a, 150);
    beep(Buzzer, aH, 500);
    beep(Buzzer, gSH, 250);
    beep(Buzzer, gH, 250);
 
    beep(Buzzer, fSH, 125);
    beep(Buzzer, fH, 125);
    beep(Buzzer, fSH, 250);
    delay(250);
    beep(Buzzer, aS, 250);
    beep(Buzzer, dSH, 500);
    beep(Buzzer, dH, 250);
    beep(Buzzer, cSH, 250);
    //repeat... repeat
 
    beep(Buzzer, cH, 125);
    beep(Buzzer, b, 125);
    beep(Buzzer, cH, 250);
    delay(250);
    beep(Buzzer, f, 250);
    beep(Buzzer, gS, 500);
    beep(Buzzer, f, 375);
    beep(Buzzer, cH, 125);
 
    beep(Buzzer, a, 500);
    beep(Buzzer, f, 375);
    beep(Buzzer, c, 125);
    beep(Buzzer, a, 1000);
}
